# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the messagelib package.
#
# SPDX-FileCopyrightText: 2023 Enol P. <enolp@softastur.org>
msgid ""
msgstr ""
"Project-Id-Version: messagelib\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-20 00:37+0000\n"
"PO-Revision-Date: 2023-10-25 10:54+0200\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: \n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.2\n"

#: src/messagepart.cpp:438
#, kde-format
msgctxt "display name for an unnamed attachment"
msgid "Unnamed"
msgstr ""

#: src/messagepart.cpp:708 src/messagepart.cpp:754 src/messagepart.cpp:1093
#, kde-format
msgid "Wrong Crypto Plug-In."
msgstr ""

#: src/messagepart.cpp:820 src/messagepart.cpp:1307
#, kde-format
msgid "No appropriate crypto plug-in was found."
msgstr ""

#: src/messagepart.cpp:822
#, kde-format
msgctxt "%1 is either 'OpenPGP' or 'S/MIME'"
msgid "No %1 plug-in was found."
msgstr ""

#: src/messagepart.cpp:825
#, kde-format
msgid "Crypto plug-in \"%1\" cannot verify signatures."
msgstr ""

#: src/messagepart.cpp:828
#, kde-format
msgid ""
"The message is signed, but the validity of the signature cannot be verified."
"<br />Reason: %1"
msgstr ""

#: src/messagepart.cpp:1309
#, kde-format
msgid "Crypto plug-in \"%1\" cannot decrypt messages."
msgstr ""

#: src/messagepart.cpp:1311
#, kde-format
msgid "Crypto plug-in \"%1\" could not decrypt the data."
msgstr ""

#: src/messagepart.cpp:1312
#, kde-format
msgid "Error: %1"
msgstr "Error: %1"
